﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    Rigidbody2D playerRigidbody;
    BoxCollider2D boxCollider2D;
    [SerializeField]
    float jumpForce = 500f;
    private bool isJumping = false;
    // Start is called before the first frame update
    void Start()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        boxCollider2D = GetComponent<BoxCollider2D>();
    }

    public void Jump()
    {
        if (!isJumping)
        {
            playerRigidbody.AddForce(new Vector2(0, jumpForce));
            isJumping = true;
        }
        
    }
    public void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Floor"))
        {
            isJumping = false;
        }
    }
}
