﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public void LoadMenuScene()
    {
        SceneManager.LoadSceneAsync(0);
    }
    public void LoadGame1Scene()
    {
        SceneManager.LoadSceneAsync(1);
    }
}
